document.querySelector(".header__nav--mobile").onclick = function () {
  const mobileNav = document.querySelector(".header__nav--mobile");
  if (mobileNav.classList.contains("mobile-open")) {
    mobileNav.classList.remove("mobile-open");
  } else {
    mobileNav.classList.add("mobile-open");
  }
};

if (document.querySelector(".slider")) {
  const slider = new A11YSlider(document.querySelector(".slider"), {});
}
